//
//  GraphContainerView.swift
//  Graph
//
//  Created by Oleksandr S. Herasymchuk on 11/29/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class GraphContainerView: UIView {
    
    // MARK: - IB Outlets
    
    @IBOutlet private var contentView: UIView!
    
    // MARK: - Private properties
    
    //constants
    private let nibName = String(describing: GraphContainerView.self)
    private let graphOffset: CGFloat = 20
    
    //graph configuration properties
    private var pointsCount = 10
    private var graphAnimationDuration = GraphAnimationDuration.Fast.rawValue
    
    //graph layers
    private var axisLayer: AnimatableLayer?
    private var graphLayer: AnimatableLayer?
    
    //calculated properties
    private var axisDrawed: Bool {
        return axisLayer != nil
    }
    
    private var maxGraphX: CGFloat {
        return (contentView.bounds.maxX - 2 * graphOffset) / 2
    }
    
    private var maxGraphY: CGFloat {
        return (contentView.bounds.maxY - 2 * graphOffset) / 2
    }
    
    // MARK: - Initializer
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadView()
    }
    
    // MARK: - Public
    
    public func changePointsCount(to value: Int) {
        pointsCount = value
        calculateAnimationDuration()
    }
    
    private func calculateAnimationDuration() {
        let duration = GraphAnimationDuration(pointsCount: pointsCount)
        graphAnimationDuration = duration.rawValue
    }
    
    public func drawGraph(redraw: Bool) {
        if !redraw && axisDrawed {
            createGraphLayer()
        } else {
            createAxisLayer()
        }
    }
    
    // MARK: - Private
    
    private func createAxisLayer() {
        graphLayer?.removeFromSuperlayer()
        axisLayer?.removeFromSuperlayer()

        let pathParams = AxisPathParams(viewRect: contentView.bounds, axisOffset: graphOffset)
        let axisPath = GraphUtils.createAxisPath(for: pathParams)
        
        let newAxisLayer = AnimatableLayer()
        contentView.layer.addSublayer(newAxisLayer)
        
        let params = AnimatableLayerPathParams(path: axisPath)
        newAxisLayer.animatePath(with: params, delegate: self)
        axisLayer = newAxisLayer
    }
    
    private func createGraphLayer() {
        graphLayer?.removeFromSuperlayer()
        
        let points = createRandomSortedPonts()
        let pathParams = GraphPathParams(points: points, viewRect: contentView.bounds)
        let graphPath = GraphUtils.createGraphLinePath(for: pathParams)
        
        let newGraphLayer = AnimatableLayer()
        contentView.layer.addSublayer(newGraphLayer)
        
        let params = AnimatableLayerPathParams(path: graphPath, animationDuration: graphAnimationDuration, lineColor: .systemRed)
        newGraphLayer.animatePath(with: params)
        graphLayer = newGraphLayer
    }
    
    private func createRandomSortedPonts() -> [GraphPoint] {
        let randomPoints = createRandomPoints()
        let sortedPonts = randomPoints.sorted { (left, right) -> Bool in
            return left.x < right.x
        }
        return sortedPonts
    }
    
    private func createRandomPoints() -> [GraphPoint] {
        var points = [GraphPoint]()
        for _ in 1...pointsCount {
            let randomPoint = createRandomPoint()
            points.append(randomPoint)
        }
        return points
    }
    
    private func createRandomPoint() -> GraphPoint {
        let randomX = CGFloat.random(in: -maxGraphX...maxGraphX)
        let randomY = CGFloat.random(in: -maxGraphY...maxGraphY)
        return GraphPoint(x: randomX, y: randomY)
    }
    
    private func loadView() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.backgroundColor = .clear
        addSubview(contentView)
    }
}

// MARK: CAAnimationDelegate

extension GraphContainerView: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            createGraphLayer()
        }
    }
}

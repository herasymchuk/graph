//
//  AnimatableLayer.swift
//  Graph
//
//  Created by Oleksandr S. Herasymchuk on 12/7/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class AnimatableLayer: CAShapeLayer {
    
    // MARK: - Private properties
    
    private let animationKey = "drawAnimation"
    private var animationDuration: CFTimeInterval = 1.0
    
    // MARK: - Initializers
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init() {
        super.init()
    }
    
    // MARK: Public methods
    
    public func animatePath(_ pathForAnimation: CGPath, for duration: CFTimeInterval = 1.0) {
        animationDuration = duration
        path = pathForAnimation
        configurePath()
        let drawAnimation = createDrawAnimation()
        add(drawAnimation, forKey: animationKey)
    }
    
//    public func drawAxis(for params: AxisLayerParams) {
//        createAxisPath(for: params)
//        let drawAnimation = createDrawAnimation()
//        add(drawAnimation, forKey: animationKey)
//    }
    
    // MARK: Private methods
    
//    private func createAxisPath(for params: AxisLayerParams) {
//        let pathParams = PathParams(viewRect: params.viewRect, axisOffset: params.axisOffset)
//        let axisPath = GraphUtils.createAxisPath(for: pathParams)
//        path = axisPath
//    }
    
    private func configurePath() {
        strokeEnd = 1
        lineWidth = 2
        lineCap = CAShapeLayerLineCap.round
        fillColor = UIColor.clear.cgColor
        strokeColor = UIColor.black.cgColor
    }
    
    private func createDrawAnimation() -> CABasicAnimation {
//        guard let animationDuration = params?.animationDuration else {
//            return CABasicAnimation()
//        }
        
        let drawAnimation = CABasicAnimation(keyPath: "strokeEnd")
        drawAnimation.fromValue = 0
        drawAnimation.toValue = 1.0175
        drawAnimation.beginTime = 0
        drawAnimation.duration = animationDuration
        drawAnimation.timingFunction = CAMediaTimingFunction(controlPoints: 0.2, 0.88, 0.09, 0.99)
        drawAnimation.fillMode = .forwards
        drawAnimation.isRemovedOnCompletion = false
        return drawAnimation
    }
}

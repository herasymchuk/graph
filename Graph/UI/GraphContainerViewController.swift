//
//  GraphContainerViewController.swift
//  Graph
//
//  Created by Oleksandr S. Herasymchuk on 11/29/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

class GraphContainerViewController: UIViewController {
    
    // MARK: - IB Outlets
    
    @IBOutlet private weak var graphContainerView: GraphContainerView!
    @IBOutlet private weak var pointsCountSlider: UISlider!
    @IBOutlet private weak var choosenPointsCountLabel: UILabel!
    @IBOutlet private weak var redrawSwitch: UISwitch!
    
    // MARK: - IB Actions
    
    @IBAction private func sliderCountChanged() {
        let currentValue = Int(pointsCountSlider.value)
        graphContainerView.changePointsCount(to: currentValue)
        choosenPointsCountLabel.text = String(currentValue)
    }
    
    @IBAction private func drawGraphTapped() {
        let redrawState = redrawSwitch.isOn
        graphContainerView.drawGraph(redraw: redrawState)
    }
}

//
//  AnimatableLayer.swift
//  Graph
//
//  Created by Oleksandr S. Herasymchuk on 12/7/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

struct AnimatableLayerPathParams {
    let path: CGPath
    var animationDuration = GraphAnimationDuration.Fast.rawValue
    var lineColor: UIColor = .black
    var lineWidth: CGFloat = 1
}

class AnimatableLayer: CAShapeLayer {
    
    // MARK: - Private properties
    
    private let animationKey = "drawAnimation"
    
    // MARK: - Initializers
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init() {
        super.init()
    }
    
    // MARK: Public methods
    
    public func animatePath(with params: AnimatableLayerPathParams, delegate: CAAnimationDelegate? = nil) {
        path = params.path
        configurePath(with: params)
        let drawAnimation = createDrawAnimation(for: params.animationDuration, delegate: delegate)
        add(drawAnimation, forKey: animationKey)
    }
    
    // MARK: Private methods
    
    private func configurePath(with params: AnimatableLayerPathParams) {
        //default
        strokeEnd = 1
        lineCap = CAShapeLayerLineCap.round
        fillColor = UIColor.clear.cgColor
        
        //changable
        lineWidth = params.lineWidth
        strokeColor = params.lineColor.cgColor
    }
    
    private func createDrawAnimation(for duration: CFTimeInterval, delegate: CAAnimationDelegate?) -> CABasicAnimation {
        let drawAnimation = CABasicAnimation(keyPath: "strokeEnd")
        drawAnimation.fromValue = 0
        drawAnimation.toValue = 1.0175
        drawAnimation.beginTime = 0
        drawAnimation.duration = duration
        drawAnimation.fillMode = .forwards
        drawAnimation.isRemovedOnCompletion = false
        drawAnimation.delegate = delegate
        return drawAnimation
    }
}

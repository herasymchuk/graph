//
//  GraphUtils.swift
//  Graph
//
//  Created by Oleksandr S. Herasymchuk on 12/7/19.
//  Copyright © 2019 Oleksandr S. Herasymchuk. All rights reserved.
//

import UIKit

enum GraphAnimationDuration: CFTimeInterval {
    case Fastest = 1
    case Fast = 1.5
    case Medium = 2
    case Slow = 2.5
    case Slowest = 4
    
    init(pointsCount: Int) {
        switch pointsCount {
        case 50..<100:
            self = .Fast
        case 100..<500:
            self = .Medium
        case 500..<900:
            self = .Slow
        case 900...1000:
            self = .Slowest
        default:
            self = .Fastest
        }
    }
}

struct AxisPathParams {
    let viewRect: CGRect
    let axisOffset: CGFloat
}

struct GraphPathParams {
    let points: [GraphPoint]
    let viewRect: CGRect
}

typealias GraphPoint = CGPoint

class GraphUtils {
    
    static func createGraphLinePath(for params: GraphPathParams) -> CGPath {
        let viewRect = params.viewRect
        let points = params.points
        
        let xLineX = viewRect.midX
        let yLineY = viewRect.midY
        
        let graphPath = CGMutablePath()
        if points.count == 1 {
            return graphPath
        }
        
        let pointOffsetTransformation = CGAffineTransform(translationX: xLineX, y: yLineY)
        let firstPoint = points[0].applying(pointOffsetTransformation)
        graphPath.move(to: firstPoint)
        
        for index in 1..<points.count {
            let currentPoint = points[index]
            let previousPoint = points[index - 1]
            
            let realCurrentPoint = currentPoint.applying(pointOffsetTransformation)
            let realPreviousPoint = previousPoint.applying(pointOffsetTransformation)
            graphPath.addLines(between: [realPreviousPoint, realCurrentPoint])
        }
        
        return graphPath
    }
    
    static func createAxisPath(for params: AxisPathParams) -> CGPath {
        let viewRect = params.viewRect
        let axisOffset = params.axisOffset
        
        let centerX = viewRect.midX
        let centerY = viewRect.midY
        let maxX = viewRect.maxX - axisOffset
        let maxY = viewRect.maxY - axisOffset
        
        let topPoint = GraphPoint(x: centerX, y: axisOffset)
        let leftPoint = GraphPoint(x: axisOffset, y: centerY)
        let rightPoint = GraphPoint(x: maxX, y: centerY)
        let bottomPoint = GraphPoint(x: centerX, y: maxY)
        
        let xLine = [leftPoint, rightPoint]
        let yLine = [topPoint, bottomPoint]
        
        let axisPath = CGMutablePath()
        axisPath.addLines(between: xLine)
        axisPath.addLines(between: yLine)
        
        return axisPath
    }
    
    static func createXYLine(for params: AxisPathParams) -> CGPath {
        let viewRect = params.viewRect
        let axisOffset = params.axisOffset
        
        let maxX = viewRect.maxX - axisOffset
        let maxY = viewRect.maxY - axisOffset
        
        let leftPoint = GraphPoint(x: axisOffset, y: maxY)
        let rigtPoint = GraphPoint(x: maxX, y: axisOffset)
        
        let xyLine = [leftPoint, rigtPoint]
        
        let axisPath = CGMutablePath()
        axisPath.addLines(between: xyLine)
        
        return axisPath
    }
}
